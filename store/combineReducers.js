import { combineReducers } from 'redux';
import auth from '@containers/login/reducer';

const appReducers = combineReducers({
  auth,
});

export default appReducers;
