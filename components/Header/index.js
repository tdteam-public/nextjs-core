import React, { Component } from 'react';
import PropsType from 'prop-types';
import { withTranslation } from '@configs/i18n.client';
import './style.scss';

class Header extends Component {
  render() {
    return (
      <header>
        <p>Header</p>
      </header>
    );
  }
}

Header.propTypes = {
  t: PropsType.func.isRequired,
};

export default withTranslation('common')(Header);
