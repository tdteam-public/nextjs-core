import React, { PureComponent } from 'react';
import PropsType from 'prop-types';
import { withTranslation } from '@configs/i18n.client';
import './style.scss';

class Footer extends PureComponent {
  render() {
    return (
      <footer>
        <p>Footer</p>
      </footer>
    );
  }
}

Footer.propTypes = {
  t: PropsType.func.isRequired,
};

export default withTranslation('common')(Footer);
