import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import combineReducers from './combineReducers';

const persistConfig = {
  key: 'td_team',
  storage,
  whitelist: ['auth'],
  blacklist: [],
};

const persistedReducer = persistReducer(persistConfig, combineReducers);

const composeEnhancers = () => {
  if (process.env.NODE_ENV === 'production') {
    return applyMiddleware(thunk);
  }
  return composeWithDevTools(applyMiddleware(thunk));
};

const store = createStore(persistedReducer, {}, composeEnhancers());

const persistor = persistStore(store);

export { store, persistor };
