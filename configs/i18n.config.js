const i18n = {
  languages: ['vi', 'en'],
  configs: {
    defaultLanguage: 'vi',
    otherLanguages: ['en'],
    localePath: 'static/locales',
    browserLanguageDetection: true,
    serverLanguageDetection: false,
    localeSubpaths: {
      vi: 'vi',
      en: 'en',
    },
    detection: {
      lookupCookie: 'language',
      order: ['cookie', 'header', 'querystring'],
      caches: ['cookie'],
    },
  },
};

// eslint-disable-next-line import/prefer-default-export
module.exports = { i18n };
