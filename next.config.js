/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
const withSass = require('@zeit/next-sass');
const withCSS = require('@zeit/next-css');
const webpack = require('webpack');
const { parsed: localEnv } = require('dotenv').config();

module.exports = withCSS(
  withSass({
    webpack(config, options) {
      config.module.rules.push({
        test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
        use: {
          loader: 'url-loader',
          options: {
            ...options,
            limit: 100000,
          },
        },
      });
      if (options.dev) {
        // eslint-disable-next-line no-param-reassign
        config.devtool = 'cheap-module-eval-source-map';
      }
      config.plugins.push(new webpack.EnvironmentPlugin(localEnv));
      return config;
    },
  }),
);
