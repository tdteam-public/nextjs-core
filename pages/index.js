import React, { Component } from 'react';
import PropsType from 'prop-types';
import { Maintenance } from '@components';
import { withTranslation } from '@configs/i18n.client';
import { APP_MODE } from '@configs';

class Index extends Component {
  render() {
    // eslint-disable-next-line no-unused-vars
    const { t } = this.props;
    if (APP_MODE === 'maintenance') {
      return <Maintenance />;
    }
    return <></>;
  }
}

Index.getInitialProps = async () => ({
  namespacesRequired: ['common'],
});

Index.propTypes = {
  t: PropsType.func.isRequired,
};

export default withTranslation('common')(Index);
