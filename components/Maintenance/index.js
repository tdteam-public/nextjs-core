import React, { Component } from 'react';
import PropsType from 'prop-types';
import Head from 'next/head';
import { Container, Row, Col } from 'reactstrap';
import { withTranslation } from '@configs/i18n.client';
import './style.scss';

class Maintenance extends Component {
  render() {
    const { t } = this.props;
    return (
      <>
        <style jsx>
          {`
            body {
              padding-top: 0 !important;
            }
          `}
        </style>
        <Head>
          <title>{t('maintenance')}</title>
        </Head>
        <div className="maintenance">
          <Container>
            <Row>
              <Col className="mx-auto">
                <img
                  src="/static/images/maintenance.jpg"
                  alt="Maintenance"
                  width="100%"
                />
                <h1>{t('maintenance')}</h1>
              </Col>
            </Row>
          </Container>
        </div>
      </>
    );
  }
}

Maintenance.propTypes = {
  footer: PropsType.string,
};

export default withTranslation('common')(Maintenance);
