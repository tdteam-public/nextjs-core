import React, { Component } from 'react';
import { BackTop } from 'antd';
import PropsType from 'prop-types';
import { Header, Footer } from '@components';

class Layout extends Component {
  render() {
    const { children } = this.props;
    return (
      <>
        <Header />
        <BackTop />
        <main>{children}</main>
        <Footer />
      </>
    );
  }
}

Layout.propTypes = {
  children: PropsType.element,
};

export default Layout;
