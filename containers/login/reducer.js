import initState from './state';

export default (state = initState, action) => {
  switch (action.type) {
    default:
      return state;
  }
};
