import NextI18Next from 'next-i18next/dist/commonjs';

const { i18n: newI18n } = require('./i18n.config');

const { languages, configs } = newI18n;

const NextI18NextInstance = new NextI18Next(configs);

NextI18NextInstance.i18n.languages = languages;

export default NextI18NextInstance;

export const {
  appWithTranslation,
  withTranslation,
  i18n,
  Link,
} = NextI18NextInstance;
