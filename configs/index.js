const { APP_MODE } = process.env;
const { SENTRY_DNS } = process.env;
const { NODE_ENV } = process.env;

export { APP_MODE, SENTRY_DNS, NODE_ENV };
