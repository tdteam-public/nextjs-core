/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import App from 'next/app';
import { Provider } from 'react-redux';
import withRedux from 'next-redux-wrapper';
import Router from 'next/router';
import NProgress from 'nprogress';
import { PersistGate } from 'redux-persist/integration/react';
import { Layout } from '@components';
import { appWithTranslation } from '@configs/i18n.client';
import { store, persistor } from '@store';
import { APP_MODE, SENTRY_DNS, NODE_ENV } from '@configs';
import * as Sentry from '@sentry/browser';
import 'antd/dist/antd.css';
import 'bootstrap/dist/css/bootstrap-grid.min.css';
import './style.scss';

if (NODE_ENV === 'production') {
  Sentry.init({ dsn: SENTRY_DNS });
}
Router.events.on('routeChangeStart', () => {
  NProgress.start();
});
Router.events.on('routeChangeComplete', () => NProgress.done());
Router.events.on('routeChangeError', () => NProgress.done());

const makeStore = () => {
  return store;
};

class TDTeam extends App {
  static async getInitialProps({ Component, ctx }) {
    return {
      pageProps: {
        ...(Component.getInitialProps
          ? await Component.getInitialProps(ctx)
          : {}),
      },
    };
  }

  render() {
    const {
      Component,
      pageProps,
      router: { route },
    } = this.props;
    const LayoutWrapper = () => {
      if (APP_MODE === 'maintenance' && route === '/') {
        return <Component {...pageProps} />;
      }
      return (
        <Layout>
          <Component {...pageProps} />
        </Layout>
      );
    };
    return (
      <Provider store={store}>
        <PersistGate loading={<LayoutWrapper />} persistor={persistor}>
          <LayoutWrapper />
        </PersistGate>
      </Provider>
    );
  }
}

export default withRedux(makeStore)(appWithTranslation(TDTeam));
