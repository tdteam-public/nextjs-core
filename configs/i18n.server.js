const NextI18Next = require('next-i18next/dist/commonjs').default;
const { i18n } = require('./i18n.config');

const { languages, configs } = i18n;

const NextI18NextInstance = new NextI18Next(configs);

NextI18NextInstance.i18n.languages = languages;

module.exports = NextI18NextInstance;
